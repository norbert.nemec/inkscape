@CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat" -arch=amd64 %*

if not exist .\.vcpkg (
    REM currently, this build requires several unmerged changes to vcpkg, so a custom repo is needed
    git clone https://github.com/NNemec/vcpkg.git .\.vcpkg
)

cd .\.vcpkg

if not exist .\vcpkg.exe (
    @CALL .\bootstrap-vcpkg.bat
)

SET VCPKG_DEFAULT_TRIPLET=x64-windows

.\vcpkg install bdwgc                ^
                gtkmm                ^
                gdl                  ^
                gsl                  ^
                libxslt              ^
                boost-optional       ^
                boost-utility        ^
                boost-concept-check  ^
                boost-math           ^
                boost-ptr-container  ^
                boost-multi-index    ^
                boost-assign         ^
                librsvg              ^
                double-conversion
