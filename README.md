Inkscape - native Windows build (WORK IN PROGRESS)
==================================================
Norbert Nemec - Jun 19, 2019

This branch of Inkscape contains the work in progress towards a native Windows build of Inkscape.

## Goal
Build Inkscape without Cygwin/MinGW. Hopefully, this will allow a smoother development experience with the native
Windows dev tools and ultimately lead to an improved integration in the Windows environment.

## Current state
After several weekends of hard work:
* Inkscape can be compiled using Visual Studio Community 2017
* The result is functional but unpolished
* Most non-essential functionality is disabled
* Many of the changes are temporary hacks that need to be cleaned up before submission.

Notice that I will rebase and reorder this branch regularly. If you want to contribute, please contact me so we can coordinate the effort.

## Build instructions
* call .\installdeps-vcpkg.bat
    * expect a few hours for all 100+ dependencies to get downloaded and compiled
    * everything will be end up in .vcpkg/ below this project root
    * the builds sometimes hang or fail and require killing and restarting the script
* open this folder in Visual Studio 2017 (free Community edition is sufficient)
    * note that VS2019 currently has an issue that causes the CMake cache to wiped out whenever CMakeFiles.txt is touched
    * CMake should automatically start creating its caches
* menu CMake|"Build All"
* choose the target "Inkscape.exe (Install)" to run the build

---
---

Inkscape. Draw Freely.
======================

[https://www.inkscape.org/](https://www.inkscape.org/)

Inkscape is an open source drawing tool with capabilities similar to
Illustrator, Freehand, and CorelDRAW that uses the W3C standard scalable
vector graphics format (SVG). Some supported SVG features include
basic shapes, paths, text, markers, clones, alpha blending, transforms,
gradients, and grouping. In addition, Inkscape supports Creative Commons
meta-data, node-editing, layers, complex path operations, text-on-path,
and SVG XML editing. It also imports several formats like EPS, Postscript,
JPEG, PNG, BMP, and TIFF and exports PNG as well as multiple vector-based
formats.

Inkscape's main motivations are to provide the Open Source community
with a fully W3C compliant XML, SVG, and CSS2 drawing tool emphasizing a
lightweight core with powerful features added as extensions, and the
establishment of a friendly, open, community-oriented development
processes.
