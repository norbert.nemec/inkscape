if(UNIX)
    #The install directive for the binaries and libraries are found in src/CMakeList.txt
    install(FILES
      ${CMAKE_BINARY_DIR}/org.inkscape.Inkscape.desktop
      DESTINATION ${SHARE_INSTALL}/applications)
    install(FILES ${CMAKE_BINARY_DIR}/org.inkscape.Inkscape.appdata.xml
      DESTINATION ${SHARE_INSTALL}/metainfo)
endif()

if(MINGW)
    include(CMakeScripts/InstallMSYS2.cmake)
endif()

if(VCPKG_TOOLCHAIN)
	# hacky solution to install pixbufloader-svg.dll, a plugin loaded at runtime to allow gdk-pixbuf to read SVG files.

	if(CMAKE_BUILD_TYPE MATCHES "^Release$")
		set(VCPKG_INSTALLED_BIN "${_VCPKG_INSTALLED_DIR}/${VCPKG_TARGET_TRIPLET}/bin")
	else()
		set(VCPKG_INSTALLED_BIN "${_VCPKG_INSTALLED_DIR}/${VCPKG_TARGET_TRIPLET}/debug/bin")
	endif()
	message("VCPKG_INSTALLED_BIN: ${VCPKG_INSTALLED_BIN}")
	file(GLOB RSVG_DLL "${VCPKG_INSTALLED_BIN}/rsvg-*.dll")
	file(GLOB CROCO_DLL "${VCPKG_INSTALLED_BIN}/croco-*.dll")
	file(GLOB PIXBUFLOADER_SVG_DLL "${VCPKG_INSTALLED_BIN}/pixbufloader-svg*.dll")
	install(FILES ${RSVG_DLL} ${CROCO_DLL} ${PIXBUFLOADER_SVG_DLL}
	        DESTINATION bin)

	string(REPLACE "${VCPKG_INSTALLED_BIN}" "${CMAKE_INSTALL_PREFIX}/bin" PIXBUFLOADER_SVG_DLL_INSTALLED ${PIXBUFLOADER_SVG_DLL})
	string(REPLACE "/" "\\\\" PIXBUFLOADER_SVG_DLL_ESCAPED ${PIXBUFLOADER_SVG_DLL_INSTALLED})
	configure_file("CMakeScripts/loaders.cache.in" "loaders.cache" @ONLY)
	install(FILES "${CMAKE_CURRENT_BINARY_DIR}/loaders.cache" DESTINATION "./lib/gdk-pixbuf-2.0/2.10.0")
endif()